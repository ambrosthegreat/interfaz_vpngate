#!/usr/bin/python3
#
# consola.py
# Librería para acceder desde un terminal a mis utilidades de red
#

# CONSTANTES
MI_PROMPT = 'Acceso vpn (escribe \'ayuda\' para opciones):'
NOM_FICH_VPN = 'servidores_vpn.csv'

# LIBRERÍAS
from mi_red import Mi_red

# FUNCIONES
# Falta implementar historial pero hay que capturar el teclado: https://www.codigopiton.com/detectar-pulsacion-de-tecla-en-python/
class Consola():
    def __init__( self ):
        # Inicialización clase
        self._mi_red = Mi_red()
        self._lista_csv = []
        self._nom_csv = '/tmp/' + NOM_FICH_VPN
        self._historial = []
        self.pais = ''
        self._indice = -1

        salir = False
        while not salir:
            entrada = self._prompt()
            salir = self._procesar_entrada( entrada )
            print()

    def _prompt( self ):
        # Petición de comando
        print( MI_PROMPT, end = ' ' )
        salida = input()
        return salida

    def _comando_erroneo( self ):
        # Qué hacer cuando no se reconoce una entrada
        print( 'Comando no reconocido, escriba \'ayuda\' para ver los comandos disponibles' )

    def _imprimir_ayuda( self ):
        # Mostrar en pantalla la ayuda
        print( 'Comandos disponibles:' )
        print( '    ayuda       Muestra esta ayuda' )
        print( '    anterior    Servidor anterior' )
        print( '    cerrar      Cerrar openvpn' )
        print( '    conectar    Conectar a un vpn' )
        print( '    descargar   Descarga el listado de servidores en el archivo csv' )
        print( '    listar      Muestra todos los servidores' )
        print( '    nom_csv     Cambiar nombre del fichero con los servidores (sin argumentos muestra el nombre)' )
        print( '    paises      Lista los países a los que se puede conectar' )
        print( '    sel_pais    Conectar a un vpn del país pasado como parámetro' )
        print( '    siguiente   Siguiente servidor' )
        print( '    salir       Sale del programa' )

    def _cambia_nom_csv( self, nomfich = '' ):
        # Cambiar nombre del fichero csv con los servidores
        if nomfich == '':
             print( 'Nombre del fichero csv: \'' + self._nom_csv + '\'' )
        else:
             self._nom_csv = nomfich
             print( 'Nombre cambiado a: \'' + self._nom_csv + '\'' )

    def _descarga_csv( self ):
        # Descarga el listado de servidores en un fichero csv
        self._mi_red.descarga_csv( nom_csv = self._nom_csv )
        self._lista_csv = self._mi_red.lee_csv( nom_csv = self._nom_csv )
        print( 'Descargado ', self._nom_csv )

    def _imprime_paises( self, paises ):
        # Imprime la lista de países
        print( 'Países: ', end = '' )
        i = 0
        for pais in paises:
            i += 1
            print( '\'' + pais['PaisLargo'] + ' (' + pais['PaisCorto'] + ')\'', end = '' )
            if i < len( paises ):
                print( ' ', end = '' )
            else:
                print()

    def _abrir_vpn( self, conf_base64 ):
		# Almacena la configuración en la cadena base64 suministrada y abre el vpn de esa configuración
        self._mi_red.guarda_fich_base64( conf_base64 )
        self._mi_red.muestra_estado()
        self._mi_red.abrir_openvpn()
        self._mi_red.muestra_estado()

    def _cerrar_vpn( self ):
        # Cierra todas las instancias de openvpn
        self._mi_red.matar_openvpn()
        self._mi_red.muestra_estado()

    def _conectar( self, pais = '' ):
        # Conectar al vpn del país pasado como argumento
        if len( self._lista_csv ) == 0:
            print( 'Lista de vpn no descargada' )
        else:
            if pais == '':
                if self._indice <0:
                    indice = 0
                    self._indice = 0
                else:
                    indice = self._indice
            else:
                indice = self._mi_red.encontrar_pais( pais, self._lista_csv )
            if indice < 0:
                print( 'País no reconocido' )
            else:
                self._indice == indice
                print( 'Conectar a (mi id = ' + str( indice ) + '): ' )
                self._mi_red.imprime_datos_vpn( self._lista_csv[indice] )
                self._abrir_vpn( self._lista_csv[indice]['Config_base64'] )

    def _cargar( self ):
        # Cargar datos de archivo
        mi_lista = self._mi_red.lee_csv( nom_csv = self._nom_csv )
        # Ordenar por velocidad
        self._lista_csv = sorted( mi_lista, key = lambda servidor : servidor['Puntuacion'], reverse = True )

    def _siguiente( self ):
        # Carga el siguiente servidor
        if self._indice < 0:
            print( 'No hay ningún servidor cargado' )
        else:
            if( self._indice >= len( self._lista_csv )-1 ):
                self._indice = 0
            else:
                self._indice = self._indice + 1
            self._cerrar_vpn()
            self._conectar()

    def _anterior( self ):
        # Carga el servidor anterior
        if self._indice < 0:
            print( 'No hay ningún servidor cargado' )
        else:
            if self.indice == 0:
                self.indice =len( self._lista_csv ) - 1
            else:
                self._indice = self._indice - 1
            self._cerrar_vpn()
            self._conectar()

    def _listar( self ):
        # Lista todos los servidores vpn
        if len( self._lista_csv ) <= 0:
            print( 'Lista de servidores no cargada' )
        else:
            print( 'Listado de servidores:' )
            for servidor in self._lista_csv:
                print( servidor['Nombre'], end = ' ' )
                print( '\'' + str( servidor['PaisLargo'] ) + '\'', end = ' ' )
                print( format( int( servidor['Velocidad'] ), ',.0f' ).replace( ',', '.' ), 'Mbps' )

    def _procesar_entrada( self, cadena ):
        # Procesamiento de comandos
        self._historial.append( cadena )

        salida = False
        if cadena == '':
            comando = ''
            argumentos = ''
        else:
            cadena = cadena.strip()
            argumentos = cadena.split()
            comando = argumentos[0]
            argumentos.pop( 0 )
            str_arg = ' '.join( argumentos )

        # print( 'Comando = \'' + comando + '\'' )
        # print( 'Argumentos = \'' + str( argumentos ) + '\'' )

        if comando == '':
            self._comando_erroneo()
        elif comando == 'ayuda':
            self._imprimir_ayuda()
        elif comando == 'cargar':
            self._cargar()
        elif comando == 'descargar':
            self._descarga_csv()
        elif comando == 'nom_csv':
            self._cambia_nom_csv( str_arg )
        elif comando == 'paises':
            self._imprime_paises( self._mi_red.paises( self._lista_csv ) )
        elif comando == 'sel_pais':
            self._conectar( str_arg )
        elif comando == 'cerrar':
            self._cerrar_vpn()
        elif comando == 'conectar':
            self._conectar()
        elif comando == 'siguiente':
            self._siguiente()
        elif comando == 'listar':
            self._listar()
        elif comando == 'anterior':
            self._anterior()
        elif comando == 'salir':
            self._cerrar_vpn()
            salida = True
        else:
            self._comando_erroneo()

        return salida
