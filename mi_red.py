#!/usr/bin/python3
#
# mired.py
# Librería con mis utilidades de red
#
# URL vpngate: https://www.vpngate.net/en/
# Ejemplo perfecto descarga vpn: https://webeduclick.com/python-script-to-connect-to-vpn/
# Obtener IP pública: https://www.codegrepper.com/profile/beautiful-bear-k4ijiv8g3hdn
# Info a partir de la IP: https://www.freecodecamp.org/news/how-to-get-location-information-of-ip-address-using-python/
#
'''
MIS NOTAS: Para hacer ficheros temporales:
    import tempfile

    _, fich_path = tempfile.mkstemp()
'''

# CONSTANTES
CSV_LIST = 'http://www.vpngate.net/api/iphone'  # URL con listado de servidores vpn
LISTA_VPN_NOMFICH = 'vpn.csv'                   # Nombre por defecto para el fichero de guardar el listado de vpns
PATH_OPENVPN = '/usr/sbin/openvpn'              # Ubicación en el sistema de openvpn
T_ESPERA_OVPN = 12                              # Tiempo de espera para terminar de abrir openvpn
T_ESPERA_MATAR = 2                              # Tiempo de espera para matar openvpn
URL_MI_IP = 'https://api.ipify.org'             # URL de la API para tener mi IP externa e info de una IP
URL_VPNGATE = 'https://www.vpngate.net/en/'     # URL de vpngate
VPN_NOMFICH = 'vpn.config'                      # Nombre por defecto fichero con un servidor para openvpn

# VARIABLES

# LIBRERÍAS
from base64 import b64decode
from csv import reader as leer_csv
from requests import get as descargar
from shlex import split
from subprocess import call, Popen
from time import sleep

# FUNCIONES
class Mi_red():
    def __init__( self ):
        # Inicialización clase
        self.mi_ip = self._mi_ip()
        self.info_mi_ip = self._info_mi_ip()

    def actualizar( self ):
        # Actualiza valor de I
        # Actualiza valor de IPP
        self.mi_ip = self._mi_ip()
        self.info_mi_ip = self._info_mi_ip()

    def descarga_csv( self, nom_csv = LISTA_VPN_NOMFICH, url = CSV_LIST ):
        # Descarga el csv de vpngate con los datos de los servidores de vpn.
        # Argumentos:
        #   nom_csv (opcional) - Nombre del fichero en el que guardar los datos.
        #   url (opcional) - Dirección alternativa de la que descargar los datos.
        fichero = descargar( url, allow_redirects = True )
        open( nom_csv, 'wb').write( fichero.content )
        fichero.close()

    def lee_csv( self, nom_csv = LISTA_VPN_NOMFICH ):
        # Procesa los datos del csv con datos de los servidores de vpn.
        # Devuelve una lista con todo. Argumentos:
        #   nom_csv (opcional) - Nombre del fichero del que descargar los datos
        lista_vpn = []
        with open( nom_csv, 'r' ) as fichero:
            datos = leer_csv( fichero, delimiter = ',' )
            indice = 0
            for fila in datos:
                if indice == 0:
                    titulo = fila[0]
                elif indice == 1:
                    encabezados = fila
                    # ['#HostName', 'IP', 'Score', 'Ping', 'Speed', 'CountryLong', 'CountryShort', _
                    #  'NumVpnSessions', 'Uptime', 'TotalUsers', 'TotalTraffic', 'LogType', 'Operator', _
                    #  'Message', 'OpenVPN_ConfigData_Base64']
                else:
                    if len( fila ) == 15:
                        servidor = {
                                'Nombre': fila[0],
                                'IP': fila[1],
                                'Puntuacion': int( fila[2] ),
                                'Ping': int( fila[3] ), #ms
                                'Velocidad': int( fila[4] )/1000000, #Mbps
                                'PaisLargo': fila[5],
                                'PaisCorto': fila[6],
                                'NumSesionesVpn': int( fila[7] ),
                                'TiempoUp': int( fila[8] ),
                                'NumUsuarios': int( fila[9] ),
                                'Trafico': int( fila[10] )//1000000000, #Gb
                                'TipoLog': fila[11],
                                'Operador': fila[12],
                                'Mensaje': fila[13],
                                'Config_base64': fila[14]
                                }
                        lista_vpn.append( servidor )
                indice += 1
            fichero.close()
        return lista_vpn

    def guarda_fich_base64( self, datos_base64, nom_fich = VPN_NOMFICH ):
        # Guarda en disco un fichero a partir de una cadena en base64.
        # Argumentos:
        #   datos_base64 - Cadena con los datos en base64.
        #   nom_fich (opcional) - Nombre del fichero en el que guardar los datos.
        fichero = open( nom_fich, 'wb' )
        fichero.write( b64decode( datos_base64 ) )
        fichero.close()

    def corrige_base64( self, nom_fich = VPN_NOMFICH ):
        # El fichero de configuración de openvpn generado contiene algunos errores.
        '''
        2022-04-17 18:38:12 DEPRECATED OPTION: --cipher set to 'AES-128-CBC' but missing in --data-ciphers (AES-256-GCM:AES-128-GCM). Future OpenVPN version will ignore --cipher for cipher negotiations. Add 'AES-128-CBC' to --data-ciphers or change --cipher 'AES-128-CBC' to --data-ciphers-fallback 'AES-128-CBC' to silence this warning.
        2022-04-17 18:38:12 WARNING: No server certificate verification method has been enabled.  See http://openvpn.net/howto.html#mitm for more info.
        2022-04-17 18:38:20 WARNING: this configuration may cache passwords in memory -- use the auth-nocache option to prevent this
        '''

    def _mi_ip( self, url_mi_ip = URL_MI_IP ):
        # Función que devuelve mi dirección IP externa. Argumentos:
        #   url_mi_ip (opcional): Dirección desde la que descargar mi IP.
        salida = descargar( url_mi_ip ).text
        return salida

    def _info_mi_ip( self, direccion_ip = '' ):
        # Función que devuelve información a partir de una IP dada. Argumentos:
        #   direccion_ip (opcional) - Si no se suministra emplea mi IP externa.
        if direccion_ip == '':
            direccion_ip = self.mi_ip
        ip_json = { 'ip': direccion_ip }
        info_ip = descargar( f'https://ipapi.co/{direccion_ip}/json/').json()
        return info_ip

    def muestra_estado( self ):
        self.actualizar()
        # Imprimir mi IP externa y a qué país pertenece
        print( 'IP = ', self.mi_ip, end = ' ' )
        print( '\tPaís = ', self.info_mi_ip['country_name'] )

    def abrir_openvpn( self,
                    nom_fich_config = VPN_NOMFICH,
                    path_openvpn = PATH_OPENVPN,
                    t_espera = T_ESPERA_OVPN):
        # Abre openvpn. Argumentos:
        #   nom_fich_config (opcional) - Nombre del fichero de configuración.
        #   path_openvpn (opcional) - Ubiocación en el sistema de openvpn (no estaba en el path por defecto).
        #   t_espera (opcional) - Tiempo que se espera a que abra openvpn.
        # Devuelve el pid del proceso de openvpn.
        proceso_vpn = Popen( split( 'sudo ' + path_openvpn + ' --config ' + nom_fich_config + ' --verb 0' ) )
        salida = proceso_vpn.pid
        sleep( t_espera )
        return salida

    def matar_openvpn( self, t_espera = T_ESPERA_MATAR ):
        # Mata openvpn. Argumentos:
        #   t_espera (opcional) - Tiempo que se espera para matar openvpn.
        # Devuelve el pid del proceso kill.
        proceso_matar = call( split( 'sudo killall openvpn' ) )
        sleep( t_espera )

    def _existe_en_lista( self, elemento, lista ):
        # Comprueba si 'elemento' existe en 'lista'
        salida = False
        for indice in lista:
            if indice == elemento:
                salida = True
                break
        return salida

    def paises( self, lista_vpn ):
        # Genera una lista de países. Argumento:
        #   lista_vpn - Lista de vpns en json
        paises = []
        for elemento in lista_vpn:
            pais = {
                    'PaisLargo': elemento['PaisLargo'],
                    'PaisCorto': elemento['PaisCorto']
                    }
            if not self._existe_en_lista( pais, paises ):
                paises.append( pais )
        return paises

    def encontrar_pais( self, cod_pais, lista_vpn ):
        # Busca un país; se puede pasar la referencia corta o la larga.
        # Devuelve el número de índice.
        encontrado = False
        indice = 0
        for elemento in lista_vpn:
            if cod_pais.lower() == elemento['PaisLargo'].lower() or cod_pais.lower() == elemento['PaisCorto'].lower():
                encontrado = True
                break
            indice += 1
        if encontrado:
            salida = indice
        else:
            salida = -1
        return salida

    def imprime_datos_vpn( self, datos_vpn ):
        # Muestra en pantalla los datos más relevantes de un vpn
        print( '\tNombre vpn:\t\t', datos_vpn['Nombre'] )
        print( '\tPuntuación:\t\t', format( datos_vpn['Puntuacion'], ',.0f' ).replace( ',', '.' ) )
        print( '\tVelocidad:\t\t', format( datos_vpn['Velocidad'], ',.0f' ).replace( ',', '.' ), 'Mbps' )
        print( '\tPing:\t\t\t', datos_vpn['Ping'], 'ms' )
        print( '\tPaís:\t\t\t', datos_vpn['PaisLargo'] )
        print( '\tNúmero de usuarios:\t', format( datos_vpn['NumUsuarios'], ',.0f' ).replace( ',', '.' ), 'usuarios' )
        print( '\tTráfico:\t\t', format( datos_vpn['Trafico'], ',.0f' ).replace( ',', '.' ), 'GB' )
        print( '\t', datos_vpn['Mensaje'] )
        print()
